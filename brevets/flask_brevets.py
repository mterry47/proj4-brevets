"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    brev_start_time = request.args.get("brev_start_time", type=str)
    brev_dist_km = request.args.get("brev_dist_km", type=float)

    message = ""

    if km > (1.2*brev_dist_km):
        message = "The control point ({} km) is over 20 percent longer than the theoretical distance ({} km). Please check units.".format(km, brev_dist_km)
    elif km < 0:
        message = "Invalid control point entered: negative number"

    # TODO: FIX FLASK FLASH MESSAGES SO THEY ACTUALLY SHOW UP WHEN HELPFUL (not when refreshing)

    open_time = acp_times.open_time(km, brev_dist_km, brev_start_time)
    close_time = acp_times.close_time(km, brev_dist_km, brev_start_time)

    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
